package repository.impl;

import java.sql.Connection;
import java.sql.SQLException;

import domain.EnumerationValue;
import repository.IEntityBuilder;
import repository.IEnumerationValueRepository;
import repository.Repository;
import uow.IUnitOfWork;

public class HsqlEnumerationValuesRepository extends Repository<EnumerationValue> implements IEnumerationValueRepository {

	public HsqlEnumerationValuesRepository(Connection connection, IEntityBuilder<EnumerationValue> builder, IUnitOfWork uow) {
		super(connection,builder,uow);
	}

	@Override
	protected void setUpUpdateQuery(EnumerationValue entity) throws SQLException {
		update.setInt(1, entity.getIntKey());
		update.setString(2, entity.getStringKey());
		update.setString(3, entity.getValue());
		update.setString(4, entity.getEnumerationName());
	}

	@Override
	protected void setUpInsertQuery(EnumerationValue entity) throws SQLException {
		insert.setInt(1, entity.getIntKey());
		insert.setString(2, entity.getStringKey());
		insert.setString(3, entity.getValue());
		insert.setString(4, entity.getEnumerationName());
	}

	@Override
	protected String getTableName() {
		return "enumerations";
	}

	@Override
	protected String getUpdateQuery() {
		return "UPDATE enumerations SET (intKey, stringKey, value, enumerationName)=(?,?,?,?) WHERE id=?;";
	}

	@Override
	protected String getInsertQuery() {
		return "INSERT INTO enumerations (intKey, stringKey, value, enumerationName) VALUES (?,?,?,?);";
	}


	public int count() {
		// TODO Auto-generated method stub
		return 0;
	}

	public EnumerationValue withName(String name) {
		// TODO Auto-generated method stub
		return null;
	}

	public EnumerationValue withIntKey(int key, String name) {
		// TODO Auto-generated method stub
		return null;
	}

	public EnumerationValue withStringKey(String key, String name) {
		// TODO Auto-generated method stub
		return null;
	}
	
}
