package repository.impl;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import repository.IRepositoryCatalog;
import uow.HsqlUnitOfWork;
import uow.IUnitOfWork;

public class RepositoryCatalogProvider {
	
private static String url="jdbc:hsqldb:hsql://localhost/workdb";
	
	public static IRepositoryCatalog catalog()
	{

		try {
			Connection connection = DriverManager.getConnection(url);
			IUnitOfWork uow = new HsqlUnitOfWork(connection);
			@SuppressWarnings("unused")
			IRepositoryCatalog catalog = new HsqlRepositoryCatalog(connection, uow);
		
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}

}
