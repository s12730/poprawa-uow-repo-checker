package repository.impl;

import java.sql.Connection;
import java.sql.SQLException;


import domain.User;
import repository.IEntityBuilder;
import repository.IUserRepository;
import repository.Repository;
import uow.IUnitOfWork;

public class HsqlUserRepository extends Repository<User> implements IUserRepository{

	protected HsqlUserRepository(Connection connection, IEntityBuilder<User> builder, IUnitOfWork uow) {
		super(connection, builder, uow);
		// TODO Auto-generated constructor stub
	}


	@Override
	protected void setUpUpdateQuery(User entity) throws SQLException {
		update.setString(1, entity.getLogin());
		update.setString(2, entity.getPassword());
		update.setInt(3, entity.getId());	
	}

	@Override
	protected void setUpInsertQuery(User entity) throws SQLException {
		insert.setString(1, entity.getLogin());
		insert.setString(2, entity.getPassword());
	}

	@Override
	protected String getTableName() {
		return "users";
	}

	@Override
	protected String getUpdateQuery() {
		return "UPDATE users SET (login,password)=(?,?) WHERE id=?";
	}

	@Override
	protected String getInsertQuery() {
		return "INSERT INTO users(login,password)"
				+ "VALUES(?,?)";
	}

	
	public int count() {
		// TODO Auto-generated method stub
		return 0;
	}

	
	public User withLogin(String login) {
		// TODO Auto-generated method stub
		return null;
	}

	
	public User withLoginAndPassword(String login, String password) {
		// TODO Auto-generated method stub
		return null;
	}

	
	public void setupPermissions(User user) {
		// TODO Auto-generated method stub
		
	}

}
