package repository.impl;

import java.sql.Connection;

import repository.IRepositoryCatalog;
import repository.IUserRepository;
import repository.IEnumerationValueRepository;
import uow.IUnitOfWork;



public class HsqlRepositoryCatalog implements IRepositoryCatalog {
	private Connection connection;
	private IUnitOfWork uow;
	
	public HsqlRepositoryCatalog(Connection connection, IUnitOfWork uow) {
		this.connection = connection;
		this.uow = uow;
	}

	
	public void saveChanges(){
		uow.saveChanges();
	}

	public IUserRepository users() {
		return new HsqlUserRepository(connection, new UserBuilder(), uow);
	}

	public IEnumerationValueRepository enumerations() {
		return null;
	}
}