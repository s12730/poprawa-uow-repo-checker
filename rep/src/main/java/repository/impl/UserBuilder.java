package repository.impl;

import java.sql.ResultSet;
import java.sql.SQLException;

import domain.User;
import repository.IEntityBuilder;

public class UserBuilder implements IEntityBuilder<User> {

	
	public User build(ResultSet rs) throws SQLException {
		User user = new User();
		user.setId(rs.getInt("id"));
		user.setLogin(rs.getString("login"));
		user.setPassword(rs.getString("password"));
		return user;
	}

}
