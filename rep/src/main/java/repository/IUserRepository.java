package repository;

import domain.User;

public interface IUserRepository extends IRepository<User> {

	public User withLogin(String login);
	public User withLoginAndPassword(String login, String password);
	public void setupPermissions(User user);
	
}
