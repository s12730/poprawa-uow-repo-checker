package domain;

import java.util.ArrayList;
import java.util.List;

public class User extends Entity {
	
	private String login;
	private String password;
	private List<UserRoles> userRoles;
	private List<RolePermissions> userPermissions;
	
	public User() {
		userRoles = new ArrayList<UserRoles>();
		userPermissions = new ArrayList<RolePermissions>();
	}

	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public List<UserRoles> getUserRoles() {
		return userRoles;
	}

	public void setUserRoles(List<UserRoles> userRoles) {
		this.userRoles = userRoles;
	}

	public List<RolePermissions> getUserPermissions() {
		return userPermissions;
	}

	public void setUserPermissions(List<RolePermissions> userPermissions) {
		this.userPermissions = userPermissions;
	}
	
	public void addUserPermission(RolePermissions userPermission) {
		this.userPermissions.add(userPermission);
	}
	
	public void removeUserPermission(RolePermissions userPermission) {
		this.userPermissions.remove(userPermission);
	}
	
	public void addUserRole(UserRoles userRole) {
		this.userRoles.add(userRole);
	}
	
	public void RemoveUserRole(UserRoles userRole) {
		this.userRoles.remove(userRole);
	}
	
	

}
