package uow;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.LinkedHashMap;
import java.util.Map;

import domain.Entity;
import domain.EntityState;

public class HsqlUnitOfWork implements IUnitOfWork {
	
	private Map<Entity, IUnitOfWorkRepository> entities;
	private Connection connection;
	
	public HsqlUnitOfWork() {
		entities = new LinkedHashMap<Entity, IUnitOfWorkRepository>();
	}
	
	public HsqlUnitOfWork(Connection connection) {
		super();
		this.connection = connection;
		
		try {
			connection.setAutoCommit(false);
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public void saveChanges() {
		
		for(Entity entity: entities.keySet())
		{
			switch(entity.getState())
			{
			case MODIFIED:
				entities.get(entity).persistUpdate(entity);
				break;
			case DELETED:
				entities.get(entity).persistDelete(entity);
				break;
			case NEW:
				entities.get(entity).persistAdd(entity);
				break;
			case UNCHANGED:
				break;
			default:
				break;}
		}
		
		try {
			connection.commit();
			entities.clear();
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public void undo() {
		entities.clear();
	}

	public void markAsNew(Entity entity, IUnitOfWorkRepository repo) {
		entity.setState(EntityState.NEW);
		entities.put(entity, repo);
	}

	public void markAsDeleted(Entity entity, IUnitOfWorkRepository repo) {
		entity.setState(EntityState.DELETED);
		entities.put(entity, repo);
	}

	public void markAsChanged(Entity entity, IUnitOfWorkRepository repo) {
		entity.setState(EntityState.MODIFIED);
		entities.put(entity, repo);
	}

	public Map<Entity, IUnitOfWorkRepository> getEntities() {
		return entities;
	}

	public void setEntities(Map<Entity, IUnitOfWorkRepository> entities) {
		this.entities = entities;
	}
	
	

}
