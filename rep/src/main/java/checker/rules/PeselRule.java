package checker.rules;

import java.text.DateFormat;
import java.text.SimpleDateFormat;

import checker.CheckResult;
import checker.ICheckRule;
import checker.RuleResult;
import domain.Person;

public class PeselRule implements ICheckRule<Person> {

	public CheckResult checkRule(Person entity) {
		
		if(entity.getPesel()== null) {
			return new CheckResult("brak numeru", RuleResult.ERROR);
		}
		
		if(entity.getPesel().equals("")) {
			return new CheckResult("brak numeru", RuleResult.ERROR);
		}
		
		if(entity.getPesel().length() != 11 ) {
			return new CheckResult("Zla dlugosc", RuleResult.ERROR);
		}
		
		if(!verifyPESEL(entity)) {
			return new CheckResult("Nieodpowiedni format, lub dane nie zgadzaja sie z data urodzenia", RuleResult.ERROR);
		}
		
		
		return new CheckResult("", RuleResult.OK);
	}

	private boolean verifyPESEL(Person entity) {
		
		if(entity.getDateOfBirth() != null) {
			
			DateFormat df = new SimpleDateFormat("YYMMdd");
			String reportDf = df.format(entity.getDateOfBirth().getTime());
			
			if(reportDf.equals(entity.getPesel().substring(0, 6))) {
				return true;
			}
			return false;
		}
		
		return false;	
	}
	
}
