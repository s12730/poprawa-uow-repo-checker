package checker.rules;

import checker.CheckResult;
import checker.ICheckRule;
import checker.RuleResult;
import domain.Person;

public class DateOfBirthRule implements ICheckRule<Person> {

	public CheckResult checkRule(Person entity) {
		if(entity.getDateOfBirth() == null) {
			return new CheckResult("brak daty urodzenia", RuleResult.ERROR);
		}
		
		if(entity.getUser().getLogin().equals("")) {
			return new CheckResult("brak daty urodzenia", RuleResult.ERROR);
		}
		
		
		return new CheckResult("", RuleResult.OK);
	}
	
}


