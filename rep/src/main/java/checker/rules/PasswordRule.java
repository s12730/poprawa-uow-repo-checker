package checker.rules;

import checker.CheckResult;
import checker.ICheckRule;
import checker.RuleResult;
import domain.Person;

public class PasswordRule implements ICheckRule<Person> {

	public CheckResult checkRule(Person entity) {
		
		
		if(entity.getUser().getPassword() == null) {
			return new CheckResult("Nie ma hasla.", RuleResult.ERROR);
		}
		
		if(entity.getUser().getPassword().equals("")) {
			return new CheckResult("Puste haslo", RuleResult.ERROR);
		}
		
		if(checkPasswordStrength(entity.getUser().getPassword()) && entity.getUser().getPassword().length() <= 5) {
			return new CheckResult("Za slabe haslo", RuleResult.ERROR);
		}
		
		
		
		return new CheckResult("", RuleResult.OK);
	}
	
	private boolean checkPasswordStrength(String password) {
        int strengthPercentage=0;
        String[] partialRegexChecks = { ".*[a-z]+.*", // lower
        ".*[A-Z]+.*", // upper
        ".*[\\d]+.*", // digits
        ".*[@#$%]+.*" // symbols
        };

            if (password.matches(partialRegexChecks[1])) {
            	 strengthPercentage+=25;
            }
            if (password.matches(partialRegexChecks[2])) {
            	 strengthPercentage+=25;
            }
            if (password.matches(partialRegexChecks[3])) {
            	 strengthPercentage+=25;
            }
            
            if(strengthPercentage<=49) {
            	return true;
            }
            
		return false;
}

}
