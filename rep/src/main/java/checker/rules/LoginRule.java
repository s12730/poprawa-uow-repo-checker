package checker.rules;

import checker.CheckResult;
import checker.ICheckRule;
import checker.RuleResult;
import domain.Person;

public class LoginRule implements ICheckRule<Person> {

	public CheckResult checkRule(Person entity) {
		
		if(entity.getUser().getLogin() == null) {
			return new CheckResult("Brak loginu.", RuleResult.ERROR);
		}
		
		if(entity.getUser().getLogin().equals("")) {
			return new CheckResult("Brak loginu.", RuleResult.ERROR);
		}
		
		
		return new CheckResult("", RuleResult.OK);
	}

}
