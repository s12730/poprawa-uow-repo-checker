package checker.rules;

import checker.CheckResult;
import checker.ICheckRule;
import checker.RuleResult;
import domain.Person;

public class NipRule implements ICheckRule<Person> {

	public CheckResult checkRule(Person entity) {
		
		if(entity.getNip() == null) {
			return new CheckResult("Brak NIP.", RuleResult.ERROR);
		}
		
		if(entity.getNip().equals("")) {
			return new CheckResult("Brak NIP.", RuleResult.ERROR);
		}
		
			
		if(entity.getNip().length() != 10) {
			return new CheckResult("Brak NIP.", RuleResult.ERROR);
		}
		
		
		if(!validate(nipDigits(entity.getNip()))) {
			return new CheckResult("Bledny numer NIP.", RuleResult.ERROR);
		}
		
	
		return new CheckResult("", RuleResult.OK);
	}
	
	
	private int[] nipDigits(String nip) {
		
		int nipDigits[] = new int[nip.length()];
		
		for(int i = 0; i < nip.length(); i ++) {
			nipDigits[i] = Integer.parseInt(nip.substring(i, i+1));
		}
	
		return nipDigits;
	}
	
	
	private boolean validate(int tab[]) {
		
		int ScaleTab[] = {6,5,7,2,3,4,5,6,7};
		int sum = 0;
		
		for(int i = 0; i < ScaleTab.length; i++) {
			
			sum += tab[i] * ScaleTab[i];
			
		}
		
		if(tab[9] == (sum % 11)) {	
			return true;		
		}
	
		return false;	
	}

}
