package checker.rules;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import checker.CheckResult;
import checker.ICheckRule;
import checker.RuleResult;
import domain.Person;

public class EmailRule implements ICheckRule<Person> {
	
	private Pattern pattern;
	private Matcher matcher;
	
	private static final String EMAIL_PATTERN = 
			"^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
			+ "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
	
	public EmailRule() {
		pattern = Pattern.compile(EMAIL_PATTERN);
	}

	public CheckResult checkRule(Person entity) {
		if(entity.getEmail() == null) {
			return new CheckResult("brak email", RuleResult.ERROR);
		}
		
		if(entity.getEmail().equals("")) {
			return new CheckResult("brak email", RuleResult.ERROR);
		}
		
		if(!validate(entity.getEmail())) {
			return new CheckResult("niepoprawny format", RuleResult.ERROR);
		}
		
		
		return new CheckResult("", RuleResult.OK);
	}
	
	
	
	public boolean validate(final String hex) {

		matcher = pattern.matcher(hex);
		return matcher.matches();

	}
	
}


