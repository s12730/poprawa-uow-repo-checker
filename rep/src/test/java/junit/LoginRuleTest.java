package junit;


import static org.junit.Assert.*;

import org.junit.Test;

import checker.CheckResult;
import checker.RuleResult;
import checker.rules.LoginRule;
import domain.Person;
import domain.User;

public class LoginRuleTest {

	LoginRule rule = new LoginRule();
	
	@Test
	public void check_login_and_return_error_if_null(){
		Person p = new Person();
		User u = new User();
		u.setLogin(null);
		p.setUser(u);
		CheckResult result = rule.checkRule(p);
		assertTrue(result.getResult().equals(RuleResult.ERROR));	
	}
	
	@Test
	public void check_login_and_return_error_if_empty(){
		Person p = new Person();
		User u = new User();
		u.setLogin("");
		p.setUser(u);
		CheckResult result = rule.checkRule(p);
		assertTrue(result.getResult().equals(RuleResult.ERROR));
		
	}
	
	@Test
	public void check_login_and_return_ok_if_not_null(){
		Person p = new Person();
		User u = new User();
		u.setLogin("MAG");
		p.setUser(u);
		CheckResult result = rule.checkRule(p);
		assertTrue(result.getResult().equals(RuleResult.OK));
		
	}
	
	

}
