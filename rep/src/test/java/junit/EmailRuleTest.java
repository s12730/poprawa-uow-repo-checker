package junit;

import static org.junit.Assert.assertTrue;

import org.junit.Test;

import checker.CheckResult;
import checker.RuleResult;
import checker.rules.EmailRule;
import domain.Person;

public class EmailRuleTest {

EmailRule rule = new EmailRule();
	
	@Test
	public void check_email_and_return_ok_if_valid(){
		Person p = new Person();
		p.setEmail("babla@wp.pl");	
		CheckResult result = rule.checkRule(p);
		assertTrue(result.getResult().equals(RuleResult.OK));	
	}
	
	
	@Test
	public void check_email_and_return_error_if_not_valid(){
		Person p = new Person();
		p.setEmail("babla");	
		CheckResult result = rule.checkRule(p);
		assertTrue(result.getResult().equals(RuleResult.ERROR));	
	}
	
}
