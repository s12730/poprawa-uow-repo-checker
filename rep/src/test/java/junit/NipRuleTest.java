package junit;

import static org.junit.Assert.assertTrue;

import org.junit.Test;

import checker.CheckResult;
import checker.RuleResult;
import checker.rules.NipRule;
import domain.Person;


public class NipRuleTest {

	NipRule rule = new NipRule();
	
	@Test
	public void check_nip_and_return_ok_if_valid(){
		Person p = new Person();
		p.setNip("2820026198");
		CheckResult result = rule.checkRule(p);
		assertTrue(result.getResult().equals(RuleResult.OK));
		
	}
	
	@Test
	public void check_nip_and_return_error_if_not_valid(){
		Person p = new Person();
		p.setPesel("735234523");
		CheckResult result = rule.checkRule(p);
		assertTrue(result.getResult().equals(RuleResult.ERROR));
		
	}
}