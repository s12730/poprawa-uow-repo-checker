package junit;

import static org.junit.Assert.assertTrue;

import org.junit.Test;

import checker.CheckResult;
import checker.RuleResult;
import checker.rules.PasswordRule;
import domain.Person;
import domain.User;

public class PasswordRuleTest {

	PasswordRule rule = new PasswordRule();
	
	@Test
	public void check_password_if_not_strength_return_error(){
		Person p = new Person();
		User u = new User();
		u.setPassword("abcdd");
		p.setUser(u);
		CheckResult result = rule.checkRule(p);
		assertTrue(result.getResult().equals(RuleResult.ERROR));	
	}
	
	@Test
	public void check_password_if_strength_return_ok(){
		Person p = new Person();
		User u = new User();
		u.setPassword("ABC_DC4");
		p.setUser(u);
		CheckResult result = rule.checkRule(p);
		assertTrue(result.getResult().equals(RuleResult.OK));	
	}
	
	@Test
	public void check_password_if_null_return_error(){
		Person p = new Person();
		User u = new User();
		u.setPassword(null);
		p.setUser(u);
		CheckResult result = rule.checkRule(p);
		assertTrue(result.getResult().equals(RuleResult.ERROR));	
	}
	
}
