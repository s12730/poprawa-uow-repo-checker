package junit;

import static org.junit.Assert.assertTrue;

import java.util.Calendar;
import java.util.GregorianCalendar;

import org.junit.Test;

import checker.CheckResult;
import checker.RuleResult;
import checker.rules.PeselRule;
import domain.Person;

public class PeselRuleTest {

PeselRule rule = new PeselRule();
	
	@Test
	public void check_pesel_if_not_valid_return_error(){
		Person p = new Person();
		p.setPesel("1111");
		CheckResult result = rule.checkRule(p);
		assertTrue(result.getResult().equals(RuleResult.ERROR));	
	}
	
	@Test
	public void check_pesel_if_valid_return_ok(){
		Person p = new Person();
		Calendar date = new GregorianCalendar(1990,9,10);
		p.setDateOfBirth(date);
		p.setPesel("90101032198");
		CheckResult result = rule.checkRule(p);
		assertTrue(result.getResult().equals(RuleResult.OK));	
	}
	
}
